package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random; 

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
		
        new RockPaperScissors().run();
        
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    int randomNumber;
    
    String playerChoice;
    String computerChoice;
    String contAnswer;

    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	
    	
    while (true) {
    	System.out.println("Let's play round " + roundCounter);
    	playerChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
    	while (!rpsChoices.contains(playerChoice)) {
    		System.out.println("I do not understand " + playerChoice + ". Could you try again?" );
    		playerChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
    		
    		}
    	
    	Random rand = new Random(); 
    	int randomNumber = rand.nextInt(3);
    	if (randomNumber == 0) {
    		computerChoice = "rock";
    	}
    	
    	
    	else if (randomNumber == 1) {
    		computerChoice = "scissors";
    	}
    	
    	else {
    		computerChoice = "paper";	
    	}
    	
    	// Game logic, computer wins 
    	if (computerChoice.equals("rock") && playerChoice.equals("scissors") 
    			|| computerChoice.equals("paper") && playerChoice.equals("rock")
    			|| computerChoice.equals("scissors") && playerChoice.equals("paper")) {
    		System.out.println("Human chose " + playerChoice + ", computer chose " + computerChoice + " Computer wins!");
    		computerScore++;
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    		
    		}
    	
    	// Game logic, human wins
    	else if (computerChoice.equals("paper") && playerChoice.equals("scissors") 
    			|| computerChoice.equals("scissors") && playerChoice.equals("rock")
    			|| computerChoice.equals("rock") && playerChoice.equals("paper")) {
    		System.out.println("Human chose " + playerChoice + ", computer chose " + computerChoice + " Human wins!");
    		humanScore++;
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    		
    	
    	}
    	else if (playerChoice.equals(computerChoice)) {
    		System.out.println("Human chose " + playerChoice + ", computer chose " + computerChoice + " It's a tie!");
    		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    		
    	}
    	
    	// Continue playing block
    	contAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
    	if (contAnswer.equals("y")) {
    		roundCounter ++;
    		continue;
    		
    		}
    	else if (contAnswer.equals("n")) {
    		System.out.println("Bye bye :)");
    		break;
    		}
    }
    	
    	}
    	
    	

    
    
    
    	
    
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}

